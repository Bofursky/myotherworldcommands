package net.myotherworld.Commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class CommandsData 
{
	private YamlConfiguration config;
	private File configFile;
	private Commands plugin;
	
	public String Permissions;
	public String Prefix;
	public String Package;
	public String Empty;
	public String Receive;
	public String Give;
	
	public CommandsData(Commands pl)
	{
		this.plugin = pl;
		
		reloadData();
	}
	
	public void reloadData()
	{
		load();
		
		this.Prefix = ChatColor.translateAlternateColorCodes('&',this.config.getString("Prefix", "Commands"));
		
		this.Give = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Give", "Otrzymales przedmioty" ));			
		this.Package = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Package", "Masz nowa paczke" ));			
		this.Empty = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Empty", "Brak przesylki" ));							
		this.Receive = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Receive", "Aby odebrac paczke wpisz /odbierz" ));							
		
		this.Permissions = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Permissions", "You do not have permission!" ));							
	}
	
	public void load()
	{
		try
		{
			if (!this.plugin.getDataFolder().exists()) 
			{
				this.plugin.getDataFolder().mkdirs();
			}
			if (!new File(this.plugin.getDataFolder(), "config.yml").exists()) 
			{
				this.plugin.saveResource("config.yml", false);
			}
			this.configFile = new File(this.plugin.getDataFolder(), "config.yml");
			this.config = YamlConfiguration.loadConfiguration(this.configFile);
		}
		catch (Exception ex)
		{
			this.plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
			return;
		}
	}
	public void save(String patch, Object value)
	{
		this.config.set(patch, value);
		try
		{
			this.config.save(this.configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
}

