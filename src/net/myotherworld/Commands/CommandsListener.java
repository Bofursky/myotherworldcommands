package net.myotherworld.Commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class CommandsListener implements Listener
{
	public Commands plugin;

	public CommandsListener(Commands pl)
	{
		this.plugin = pl;

	}
	
	@EventHandler
	public void CheckCommands (PlayerJoinEvent e) 
	{
		final Player p = e.getPlayer();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
		    public void run()
		    {			
				if(CommandsList.config.getString("Commands." + p.getName().toUpperCase()) != null)
				{
					p.sendMessage(Commands.data.Prefix + Commands.data.Package);
				}
		    }
		   }, 100L);
	}
}
