package net.myotherworld.Commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

public class CommandsList 
{
	public static YamlConfiguration config;
	public static File configFile;
	public Commands plugin;
		  
	public CommandsList(Commands pl)
	{
		this.plugin = pl;
		load();
	}

	public void load()
	{
		try
		{
			if (!this.plugin.getDataFolder().exists()) 
			{
				this.plugin.getDataFolder().mkdirs();
			}
			if (!new File(this.plugin.getDataFolder(), "Commands.yml").exists()) 
			{
				this.plugin.saveResource("Commands.yml", false);
			}
			CommandsList.configFile = new File(this.plugin.getDataFolder(), "Commands.yml");
			CommandsList.config = YamlConfiguration.loadConfiguration(CommandsList.configFile);
		}
		catch (Exception ex)
		{
			this.plugin.getLogger().severe("Blad podczas wczytywania pliku Commands!");
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
			return;
		}
	}
	public void save(String patch, Object value)
	{
		CommandsList.config.set(patch, value);
		try
		{
			CommandsList.config.save(CommandsList.configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

