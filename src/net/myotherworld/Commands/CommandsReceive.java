package net.myotherworld.Commands;

import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandsReceive implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldCommands.receive")) 
    	{	
			if (args.length == 0) 				
			{	
				Player p = (Player)sender;
				String player = p.getName().toUpperCase();
				if(CommandsList.config.getString("Commands." + player) != null)
				{
					List<String> list = CommandsList.config.getStringList("Commands." + player);
					
					for(String command : list)
					{
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
						
					}			
								
					try //Zapis do Cfg
					{
						CommandsList.config.set("Commands." + player, null);	
						CommandsList.config.save(CommandsList.configFile);
					} 
					catch (IOException io) 
					{
						Bukkit.getServer().getLogger().severe("Blad podczas zapisu pliku Commands!");
					}             
					sender.sendMessage(Commands.data.Prefix + Commands.data.Give);
					return true;
				}
				else
				{
					sender.sendMessage(Commands.data.Prefix + Commands.data.Empty);
				}
					
			}
			else
			{
				sender.sendMessage(Commands.data.Prefix + Commands.data.Receive);
			}
    	}
		else
		{
			sender.sendMessage(Commands.data.Prefix + Commands.data.Permissions);
		}
		return true;
	}
}
