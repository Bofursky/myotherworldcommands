package net.myotherworld.Commands;




import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandsCMD implements CommandExecutor
{
	public Commands plugin;

	public CommandsCMD(Commands pl)
	{
		this.plugin = pl;

	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, final String[] args) 
	{		
    	if(sender.hasPermission("MyOtherWorldCommands.cmd")) 
    	{	
			if (args.length > 0) 				
			{				
				String player = args[0].toUpperCase();				//Nazwa gracza z duzych liter		
                StringBuilder str = new StringBuilder();      
                for (int i = 1; i < args.length; i++) 
                {
                        str.append(args[i] + " ");
                }
                String CMD = str.toString();           				//Wychwycenie komendy             
                if(CommandsList.config.getString("Commands." + player) == null)//Wykonanie kodu gdy gracza nie ma w cfg
                {
                	List<String> CmdList = Arrays.asList(CMD);			//Zmiana w liste 
                	CommandsList.config.set("Commands." + player , CmdList);
                	sender.sendMessage(Commands.data.Prefix + "Dla gracza " + player + " dodano komende: "+ CMD);
                	
                	if(Bukkit.getPlayer(args[0]) != null)
                	{            			
                		Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
                		{
                		    public void run()
                		    {			
                		    	Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
                				targetPlayer.sendMessage(Commands.data.Prefix + Commands.data.Package);
                		    }
                		   }, 100L);
                	}
                }
                else //jesli jest 
                {
                	List<String> list = CommandsList.config.getStringList("Commands." + player);
                	list.add(CMD);
                	CommandsList.config.set("Commands." + player, list);
                	sender.sendMessage(Commands.data.Prefix + "Dla gracza " + player + " dodano komende: "+ CMD);
                	
                	if(Bukkit.getPlayer(args[0]) != null)
                	{            			
                		Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
                		{
                		    public void run()
                		    {			
                		    	Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
                				targetPlayer.sendMessage(Commands.data.Prefix + Commands.data.Package);
                		    }
                		   }, 100L);
                	}
                }
				try //Zapis do Cfg
				{
					CommandsList.config.save(CommandsList.configFile);
				} 
				catch (IOException io) 
				{
					Bukkit.getServer().getLogger().severe("Blad podczas zapisu pliku Commands!");
				}                
				return true;
			}
    	}
		else
		{
			sender.sendMessage(Commands.data.Prefix + Commands.data.Permissions);
		}
		return true;
	}


}