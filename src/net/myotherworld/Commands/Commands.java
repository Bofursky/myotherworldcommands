package net.myotherworld.Commands;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Commands extends JavaPlugin
{
	public static CommandsList config;
	public static CommandsData data;

    private void listeners()
    {
    	PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new CommandsListener(this), this);
    }
    public void onEnable() 
    { 
    	config = new CommandsList(this);	
    	data = new CommandsData(this);
    	
    	listeners();
    	
    	getCommand("CMD").setExecutor( new CommandsCMD(this) );
    	getCommand("Receive").setExecutor( new CommandsReceive() );
    
    }
}
